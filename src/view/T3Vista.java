package view;

import java.util.Scanner;

import controller.T3Controller;

public class T3Vista {
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		T3Controller controlador = new T3Controller();
		
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					System.out.println("Ingrese la expresi�n: ");
					String expresion = sc.next();
					System.out.println(controlador.expresionBienFormada(expresion));
					System.out.println(controlador.ordenarPila(expresion));
					break;
				case 2:
					fin = true;
					sc.close();
					break;
			}
		}
	}
	
	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Ingresar expresi�n");
		System.out.println("2. Salir");
		System.out.println("Seleccione una opci�n y presione enter (e.j., 1):");
		
	}
}