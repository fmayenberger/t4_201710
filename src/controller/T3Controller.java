package controller;

import model.data_structures.Stack;
import model.logic.T3Logica;

public class T3Controller {
	T3Logica manejador = new T3Logica();
	boolean bienFormada = false;
	public String expresionBienFormada(String expresion) {
		bienFormada = manejador.expresionBienFormada(expresion);
		if (bienFormada) {
			return "La expresión esta bien formada";
		} else return "La expresión no cumple con los parámetros establecidos";
	}
	
	public String ordenarPila(String expresion) {
		String retorno = "\n";
		Stack<Character> pila = new Stack<>();
		if (bienFormada) {
			char caracteresPila[] = expresion.toCharArray();
			for (Character c : caracteresPila) {
				pila.push(c);
			}
			pila = manejador.ordenarPila(pila);
		}
		while (!pila.isEmpty()) {
			retorno += (pila.pop() + "\n");
		}
		return retorno;
	}
}