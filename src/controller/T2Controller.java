package controller;

import model.data_structures.ILista;
import model.logic.T2ManejadorPeliculas;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class T2Controller {

	
	private static IManejadorPeliculas manejador= new T2ManejadorPeliculas();

	public static void cargarPeliculas() {
		
		manejador.cargarArchivoPeliculas("data/movies.csv");
		
	}

	public static ILista<VOPelicula> darListaPeliculas(String busqueda) {
		return manejador.darListaPeliculas(busqueda);
	}

	public static ILista<VOPelicula> darPeliculasPorAgno(int agno) {
		return manejador.darPeliculasAgno(agno);
	}
	
	public static VOAgnoPelicula darPeliculasAgnoSiguiente() {
		return manejador.darPeliculasAgnoSiguiente();
	}
	
	public static VOAgnoPelicula darPeliculasAgnoAnterior() {
		return manejador.darPeliculasAgnoAnterior();
	}

}
