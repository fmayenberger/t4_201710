package model.data_structures;

public class Stack<T> {
	private ListaDobleEncadenada<T> lista;
	
	public Stack() {
		lista = new ListaDobleEncadenada<T>();
	}
	
	public void push(T item) {
		lista.agregarElementoFinal(item);
	}
	
	public T pop() {
		int tam = lista.darNumeroElementos();
		T elem = lista.darElemento(tam);
		lista.remove(tam);
		return elem;
	}
	
	public boolean isEmpty() {
		return lista.isEmpty();
	}
	
	public int size() {
		return lista.darNumeroElementos();
	}
}