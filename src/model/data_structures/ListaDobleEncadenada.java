package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private Nodo<T> cabeza;
	private Nodo<T> posicionActual;
	private int tama�o;
	
	public ListaDobleEncadenada (T primerElemento){
		cabeza = new Nodo<T>(primerElemento);
		posicionActual = cabeza;
		tama�o = 1;
	}
	
	public ListaDobleEncadenada (){
		cabeza = null;
		posicionActual = null;
		tama�o = 0;
	}

	public boolean remove(int pos) {
		Nodo<T> elem = darNodo(pos);
		boolean logro = false;
		if (tama�o > 1) {
			if(elem.darAnterior() == null) {
				elem.darSiguiente().cambiarAnterior(null);
				cabeza = elem.darSiguiente();
				elem.cambiarSiguiente(null);
				logro = true;
			} else if (elem.darSiguiente() == null) {
				elem.darAnterior().cambiarSiguiente(null);
				elem.cambiarAnterior(null);
				logro = true;
			} else if (elem.darSiguiente() != null && elem.darAnterior() != null) {
				elem.darAnterior().cambiarSiguiente(elem.darSiguiente());
				elem.darSiguiente().cambiarAnterior(elem.darAnterior());
				elem.cambiarAnterior(null);
				elem.cambiarSiguiente(null);
				logro = true;
			}
		}
		else {
			cabeza = null;
			logro = true;
		}
		if (logro) tama�o--;
		return logro;
	}

	public Nodo<T> darNodo(int pos) {
		Nodo<T> actual = cabeza;
		for (int i = 0; i < pos-1; i++) {
			if(actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			else return null;
		}
		return actual;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			Nodo<T> actual = null;
			@Override
			public boolean hasNext() {
				if (actual == null || actual.darSiguiente() != null) return true;
				else return false;
			}

			@Override
			public T next() {
				if (actual == null) actual = cabeza;
				else actual = actual.darSiguiente();
				return actual.darObjeto();
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(cabeza==null){
			cabeza = new Nodo<T>(elem);
			posicionActual = cabeza;
		}
		else {
			Nodo<T> actual = cabeza;
			while (actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
			}
			Nodo<T> siguiente = new Nodo<T>(elem, actual);
			actual.cambiarSiguiente(siguiente);
		}
		tama�o++;
	}

	@Override
	public T darElemento(int pos) {
		if (tama�o > 0) {
			Nodo<T> actual = cabeza;
			for (int i = 0; i < pos-1; i++) {
				if(actual.darSiguiente() != null) {
					actual = actual.darSiguiente();
				}
				else return null;
			}
			return actual.darObjeto();
		} else return null;
	}


	@Override
	public int darNumeroElementos() {
		return tama�o;
	}

	@Override
	public T darElementoPosicionActual() {
		return posicionActual.darObjeto();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		if (posicionActual.darSiguiente() != null) {
			posicionActual = posicionActual.darSiguiente();
			return true;
		}
		else return false;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		if (posicionActual.darAnterior() != null) {
			posicionActual = posicionActual.darAnterior();
			return true;
		}
		else return false;
	}
	
	public boolean isEmpty()
	{
		return tama�o ==0;
	}
}
