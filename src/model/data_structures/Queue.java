package model.data_structures;

public class Queue<T> {
	private ListaDobleEncadenada<T> lista;
	
	public Queue(){
		lista = new ListaDobleEncadenada<T>();
	}
	
	public void enqueue(T item) {
		lista.agregarElementoFinal(item);
	}
	
	public T dequeue() {
		T elem = lista.darElemento(0);
		lista.remove(0);
		return elem;
	}
	
	public boolean isEmpty() {
		return lista.isEmpty();
	}
	
	public int size() {
		return lista.darNumeroElementos();
	}
}
