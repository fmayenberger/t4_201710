package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import api.IManejadorPeliculas;

public class T2ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;
	
	private ILista<VOAgnoPelicula> peliculasAgno;

	
	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		
		String temp = null;
		String[] temp2 = new String[3];
		
		String nombrePelicula = null;
		int a�oPelicula;
		ListaEncadenada<String> listaGeneros;
		
		misPeliculas = new ListaEncadenada<VOPelicula>();
		VOPelicula pelicula = new VOPelicula();
		int numGeneros;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(archivoPeliculas));
			//primera linea no tiene informacion
			br.readLine();
			
			while((temp = br.readLine()) != null) {

				//revisa si tiene " en el nombre
				if(temp.contains("\"")) {
					temp2[0] = temp.substring(0, temp.indexOf(','));
					temp2[1] = temp.substring(temp.indexOf(',')+2, temp.lastIndexOf(',')-1);
					temp2[2] = temp.substring(temp.lastIndexOf(',')+1, temp.length());
				}
				else {
					temp2 = temp.split(",");
				}
				
				//Saca los atributos de la pelicula
				nombrePelicula = temp2[1];
				
				try {
					a�oPelicula = Integer.parseInt(nombrePelicula.substring(nombrePelicula.lastIndexOf('(')+1, nombrePelicula.lastIndexOf(')')));
				} catch (NumberFormatException | StringIndexOutOfBoundsException e) {
					if (nombrePelicula.endsWith(")")) {
						a�oPelicula = Integer.parseInt(nombrePelicula.substring(nombrePelicula.lastIndexOf('(')+1, nombrePelicula.lastIndexOf('-')));
					} else a�oPelicula = 0;
				}
				if (nombrePelicula.contains("(") && nombrePelicula.matches(".*\\d+.*")) nombrePelicula = nombrePelicula.substring(0, nombrePelicula.lastIndexOf('(')-1);
				
				numGeneros = 1;
				for (int i = 0; i < temp2[2].length(); i++) {
					if(temp2[2].charAt(i) == '|') numGeneros++;
				}
				String[] temp3 = new String[numGeneros];
				temp3 = temp2[2].split("\\|");
				listaGeneros = new ListaEncadenada<String>();
				for (int i = 0; i < temp3.length; i++) {
					listaGeneros.agregarElementoFinal(temp3[i]);
				}
				
				//agrega la pelicula a la lista
				pelicula = new VOPelicula();
				pelicula.setAgnoPublicacion(a�oPelicula);
				pelicula.setTitulo(nombrePelicula);
				pelicula.setGenerosAsociados(listaGeneros);
				
				//System.out.println(nombrePelicula + ": " + a�oPelicula + ": " + listaGeneros.darElemento(0));
				misPeliculas.agregarElementoFinal(pelicula);
			}
			br.close();
		} catch (IOException e) {
			
		}
		
		
		VOAgnoPelicula a�o;
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
		for (int i = 0; i < 67; i++) {
			a�o = new VOAgnoPelicula();
			a�o.setAgno(1950 + i);
		
			
			ILista<VOPelicula> tempo ;
			tempo = new ListaDobleEncadenada<VOPelicula>();
			for( VOPelicula iter : misPeliculas ){
				if(iter.getAgnoPublicacion() == 1950 + i){
					tempo.agregarElementoFinal(iter);
				}
			}
			
			a�o.setPeliculas(tempo);
			peliculasAgno.agregarElementoFinal(a�o);
		}
	}


	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		ILista<VOPelicula> retorno = new ListaEncadenada<VOPelicula>();
		
		for(VOPelicula pel : misPeliculas){
			if(pel.getTitulo().toLowerCase().contains(busqueda.toLowerCase()))
			{
				retorno.agregarElementoFinal(pel);
			}
			
		}
		return retorno;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		
		for(VOAgnoPelicula ano : peliculasAgno){
			if(ano.getAgno() == agno ){
				return ano.getPeliculas();
			}
		}
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		if (peliculasAgno.avanzarSiguientePosicion()) return peliculasAgno.darElementoPosicionActual();
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		if (peliculasAgno.retrocederPosicionAnterior()) return peliculasAgno.darElementoPosicionActual();
		return null;
	}

}
