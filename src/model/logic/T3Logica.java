package model.logic;

import model.data_structures.Queue;
import model.data_structures.Stack;


public class T3Logica {
	
	public boolean expresionBienFormada(String expresion) {
		
		Stack<Character> prueba = new Stack<>();
		boolean bien = false;
		char caracteres[] = expresion.toCharArray();
		
		if(expresion.contains("(") || expresion.contains("[") || expresion.contains(")") || expresion.contains("]")) {
			for (Character c : caracteres) {
				if (c == '[') {
					prueba.push(c);
				} else if (c == '(') {
					prueba.push(c);
				} else if (c == ')') {
					Character temp = prueba.pop();
					if (temp == null || temp != '(') {
						bien = false; 
						break;
					} else bien = true;
				} else if (c == ']') {
					Character temp = prueba.pop();
					if (temp == null || temp != '[') {
						bien = false; 
						break;
					} else bien = true;
				}
			}
		} else bien = true;
		return bien;
	}
	
	public Stack<Character> ordenarPila(Stack<Character> pila) {
		String operadores = "";
		String par = "";
		Queue<Character> temp = new Queue<>();
		while (!pila.isEmpty()) {
			Character c = pila.pop();
			if (c == '[' || c == '(' || c == ')' || c == ']') {
				par += c;
			} else if (c == '*' || c == '+' || c == '-' || c == '/') {
				operadores += c;
			} else if (c != ' ') {
				temp.enqueue(c);
			}
		}
		if(operadores.length() > 0) {
			for (int i = 0; i < operadores.length(); i++) {
				temp.enqueue(operadores.charAt(i));
			}
		}
		if(par.length() > 0) {
			for (int i = 0; i < par.length(); i++) {
				temp.enqueue(par.charAt(i));
			}
		}
		while (!temp.isEmpty()) {
			pila.push(temp.dequeue());
		}
		return pila;
	}
}
